import 'package:flutter/material.dart';
import 'package:flutter_login_page/features/login_ui/presentation/pages/login_screen.dart';
import 'package:flutter_login_page/features/login_ui/presentation/pages/registro_datos.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'flutter Login UI',
      debugShowCheckedModeBanner: false,
      initialRoute: LoginScreen.ROUTE,
      routes:{ 
        LoginScreen.ROUTE: (_)=> LoginScreen(),
        RegistroDatos.ROUTE: (_)=>RegistroDatos()
      }
    );
  }
}