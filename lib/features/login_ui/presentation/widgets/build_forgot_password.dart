import 'package:flutter/material.dart';
import 'package:flutter_login_page/features/login_ui/data/datasources/constants.dart';

class BuildForgotPassword extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () => print('recuperar contraseña'),
        child: Text(
          '¿Has olvidado tu contraseña?',
          style: klabelStyle,
        ),
      ),
    );
  }
}