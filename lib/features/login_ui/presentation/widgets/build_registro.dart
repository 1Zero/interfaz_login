import 'package:flutter/material.dart';
import 'package:flutter_login_page/features/login_ui/presentation/pages/registro_datos.dart';

class BuildRegistro extends StatelessWidget { 

  @override
  Widget build(BuildContext context) {
     return         GestureDetector(
                        onTap: (){Navigator.pushNamed(context, RegistroDatos.ROUTE);},
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: '¿No tiene una cuenta?',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold
                                )
                              ),
                              TextSpan(
                                text: '  Registrate',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold
                                )
                              )
                            ]
                          )
                        
                        ),
                      );
  }
}
