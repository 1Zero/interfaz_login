import 'package:flutter/material.dart';

class BuildLogin extends StatelessWidget {  

  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () => print('iniciar sesion'),
        child: Text(
          'Acceder',
          style: TextStyle(
              color: Color(0xFF527DAA),
              fontSize: 18.0,
              letterSpacing: 1.5,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans'),
        ),
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(15.0),
            primary: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            )),
      ),
    );
  }
}