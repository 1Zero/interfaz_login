import 'package:flutter/material.dart';

class BuildSocial extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          GestureDetector(
            onTap: () => print('login con facebook'),
            child: Container(
              height: 60.0,
              width: 60.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2),
                        blurRadius: 6.0),
                  ],
                  image: DecorationImage(
                      image: AssetImage('assets/logos/facebook.jpg'))),
            ),
          ),
          GestureDetector(
            onTap: () => print('login con google'),
            child: Container(
              height: 60.0,
              width: 60.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2),
                        blurRadius: 6.0),
                  ],
                  image: DecorationImage(
                      image: AssetImage('assets/logos/google.jpg'))),
            ),
          )
        ],
      ),
    );
  }
}