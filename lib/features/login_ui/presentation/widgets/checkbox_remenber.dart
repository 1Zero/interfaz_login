import 'package:flutter/material.dart';
import 'package:flutter_login_page/features/login_ui/data/datasources/constants.dart';

class CheckboxRemenber extends StatefulWidget {
 

  @override
  _CheckboxRemenberState createState() => _CheckboxRemenberState();
}

class _CheckboxRemenberState extends State<CheckboxRemenber> {
  bool? _remenberMe = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(children: <Widget>[
      Theme(
        data: ThemeData(unselectedWidgetColor: Colors.white),
        child: Checkbox(
          value: _remenberMe,
          checkColor: Colors.green,
          activeColor: Colors.white,
          onChanged: (value) {
            setState(() {
              _remenberMe = value;
            });
          },
        ),
      ),
      Text(
        'Recuerdame',
        style: klabelStyle,
      )
    ]));
  }
}