import 'package:flutter/material.dart';
import 'package:flutter_login_page/features/login_ui/data/datasources/constants.dart';

class BuildSignin extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          '-- O --',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: 20.0,
        ),
        Text(
          'Inicia Sesion con',
          style: klabelStyle,
        )
      ],
    );
  }
}